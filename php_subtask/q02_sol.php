<?php
/*
* INFO/CS 1300
* Fall 2016
* Isabelle De Brabanter
* 
* Assignment 8, question 2
*
*/

// variables
$i; // counter variable
$j; // counter variable
$k; // counter variable
$temp_num; // holder for random value
$number_array = []; // array of random integers 1-5
$high_array = []; // integers from $number_array greater than 3

function make_randoms($number_array) {
    for ($i = 0; $i < 10; $i++) {
        $temp_num = rand(1,5);
        array_push($number_array, $temp_num);
        }
    for ($j = 0; $j < sizeof($number_array); $j++) {
        echo "$j: $number_array[$j] <br>";
    }
    return $number_array;
    }
    
function get_and_sort($number_array) {
    $high_array = make_randoms($number_array);
    
    echo "<br>";
    
    for ($k = 0; $k < sizeof($high_array); $k++) {
        if ($high_array[$k] > 3) {
            echo "<br> $k: $high_array[$k]";
            }
        }
        
    return $high_array;
}

get_and_sort($number_array);

?>