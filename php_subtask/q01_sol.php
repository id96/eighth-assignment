<?php
/*
* Isabelle De Brabanter 
* INFO/CS 1300
* Fall 2016
*
* Assignment 8, question 1
*
*/

// variables
$i; // counter variable
$key; // holder variable for array
$value; // holder variable for array
$plain_array = array(1,2,3,"four");
$assoc_array = array("Brady"=>12, "Blount"=>29, "Garropolo"=>10);


function loop_one() {
    $plain_array = array(1,2,3,"four");
    echo "Loop 1: <br>";

    // TO DO: Use a FOR LOOP to loop through this array and 
    // print out all the numbers, each in a separate line. 
    
    for ($i = 0; $i < sizeof($plain_array); $i++) {
        echo "i: $plain_array[$i] <br>";
    }
}

loop_one();
echo "<br><br>";

function loop_two() {
    $plain_array = array(1,2,3,"four");
    echo "Loop 2: <br>";
    
    foreach ($plain_array as $key => $value) {
        echo "value: $value <br>";
    
    }
}
loop_two();
echo "<br><br>";

function loop_three() {
    $plain_array = array(1,2,3,"four");
    echo "Loop 3: <br>";
    
    for ($i = 0; $i < sizeof($plain_array); $i++) {
        if (is_int($plain_array[$i])) {
            echo "value: $plain_array[$i] <br>";
        }
        
    }
}
loop_three();
echo "<br><br>";

function loop_four() {
    $assoc_array = array("Brady"=>12, "Blount"=>29, "Garropolo"=>10);
    echo "Loop 4: <br>";

    foreach ($assoc_array as $key => $value) {
        echo "$key: $value <br>";
    }
}
loop_four();

?>