<!DOCTYPE HTML>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title> The French Revolution</title>
    <link rel="stylesheet" href="css/stylesheet08.css" type= "text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
</head>

<body>
    <?php include 'header.php';?> 
    <div id="main_content">
            <div id="index_header"> 
                <h1>Le XVIII Siècle en France - The 18th Century in France</h1>
            </div> 
            <!--end of header div-->
                
            <div class="leftcol">
                <figure> <img class='img' src="images/bastille.jpg" alt='Storming of the Bastille'>
                <!-- Media Credit: https://upload.wikimedia.org/wikipedia/commons/4/4e/Prise_de_la_Bastille.jpg -->
                <figcaption class='fig_caption'>The Storming of the Bastille; July, 1789</figcaption></figure>
                
        
                <p>Other resources:</p>
                    <ul>
                        <li><a href= "http://www.history.com/topics/french-revolution">History Channel Site</a></li>
                        <li> <a href= "https://www.youtube.com/watch?v=lTTvKwCylFY">John Green Crash Course</a></li>
                    </ul>
            </div> 
            <!--end of leftcol div-->
        
            <div class= "rightcol">
                <figure><img class='img' src="images/liberte.jpg" alt='Liberty who Guides the People'>
                <!--Media Credit: https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/Eugène_Delacroix_-_La_liberté_guidant_le_peuple.jpg/970px-Eugène_Delacroix_-_La_liberté_guidant_le_peuple.jpg-->
                <figcaption class='fig_caption'> La liberté guidant le peuple (Liberty Who Guides the People)</figcaption></figure>
            </div>
            <!--end of rightcol div-->
    </div> 
    <!-- end of main content  div-->
     
</body>


</html>