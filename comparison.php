<!DOCTYPE HTML>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title> The French Revolution</title>
    <link rel="stylesheet" href="css/stylesheet08.css" type= "text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
</head>

<body>
    <?php include 'header.php';?> 
    <div id="main_content">
        <h1 id="index2_header">Comparison between French & American Revolution</h1>
            
        <div class='leftcol'>
            <div class="table">
                <table>
                    <tr>
                        <td></td>
                        <th>American</th>
                        <th>French</th>
                    </tr>
                    <tr>
                        <td>Time Period</td>
                        <td>1775-1783</td>
                        <td>1789-late 1790's</td>
                    </tr>
                    <tr>
                        <td>Leading Philosophers</td>
                        <td>Locke, Paine, Otis</td>
                        <td>Rousseau, Voltaire, Robespierre</td>
                    </tr>
                    <tr>
                        <td>Successful?</td>
                        <td>Yes</td>
                        <td>Somewhat</td>
                    </tr>
                </table>
                
                <p>American versus French</p>
            </div>
            <!--end of table div-->
        </div>
        <!--end of leftcol div-->
        
        <div class='rightcol'>
            <figure><img id='img' src=images/washington.jpg alt="Washington Crossing the Delaware">
            <!-- Media Credit: https://upload.wikimedia.org/wikipedia/commons/f/f7/Emanuel_Leutze_(American,_Schwäbisch_Gmünd_1816–1868_Washington,_D.C.)_-_Washington_Crossing_the_Delaware_-_Google_Art_Project.jpg--><figcaption class='fig_caption'>Washington Crossing the Delaware</figcaption></figure>
        </div>
        <!--end of rightcol div-->
    </div> 
    <!-- end of main content  div-->
     
</body>
</html>